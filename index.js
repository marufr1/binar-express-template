const express = require('express');
// const session = require('express-session');
const cookieParser = require('cookie-parser');
const cors = require('cors');
// const session_conf = require('./config/session');
const routes = require('./app/routes');
const config = require('./config');
const { Sequelize } = require('sequelize');
const logger = require('morgan');

const app = express();
appInit(app);

app.listen(config.PORT, () => {
    console.log(`Listening on port 3000..`);
    const db = new Sequelize(
      config.DB_NAME,
      config.DB_USERNAME,
      config.DB_PASSWORD,
      {
        host: config.DB_HOST,
        port: config.DB_PORT,
        dialect: config.DB_DIALECT,
        logging: false
      }
    );
    db.authenticate()
    .then(() => {
        console.log('Successfully connected to Local database');
    })
    .catch(err => {
        console.error('Failed to connect into Local database:', err);
        process.exit();
    });
});

function appInit(app){
    app.use(logger('dev'));
    app.use(cookieParser('secret'));
    // app.use('/admin/assets', express.static('assets'));
    // app.use('/admin/assets', express.static('views/assets'));
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    // app.use(session({
    //     secret: session_conf.secret,
    //     resave: false,
    //     saveUninitialized: false,
    // }));
    app.set('view engine' , 'ejs');
    app.use('/', routes);
    process.env.TZ = 'Asia/Jakarta';
}